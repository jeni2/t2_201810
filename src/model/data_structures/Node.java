package model.data_structures;
import java.sql.Date;

import com.sun.javafx.scene.paint.GradientUtils.Point;

import api.*;
import javafx.scene.chart.PieChart.Data;
public class Node {
	
	//Atributos//
	
	private String compa�ia;
	private double dropoff_census_tract;
	private double dropoff_centroid_latitude;
	private Point dropoff_centroid_location;
	private double dropoff_centroid_longitude;
	private double dropoff_community_area;
	private double extras;
	private double fare;
	private String payment_type;
	private double pickup_census_tract;
	private double pickup_centroid_latitude;
	private Point pickup_centroid_location;
	private double pickup_centroid_longitude;
	private double pickup_community_area;
	private String taxi_id;
	private double tips;
	private double tolls;
	private Date trip_end_timestamp;
	private String trip_id;
	private double trip_miles;
	private Date trip_seconds;
	private String trip_start_timestamp;
	private double trip_total;
	
	//Metodods//
	
	public String getCompa�ia(){
		return compa�ia;
	}
	
	public double getCensusTrac(){
		return dropoff_census_tract;		
	}
	public double getCentroidLatitude(){
		return dropoff_centroid_latitude;		
	}
	public Point getCentroidLocation(){
		return dropoff_centroid_location;		
	}
	public double getCentridLongitude(){
		return dropoff_centroid_longitude;		
	}
	public double getCommunityArea(){
		return dropoff_community_area;		
	}
	public String getTaxiId(){
		return taxi_id;
	}
	public double gettips(){
		return tips;		
	}
	public double tolls(){
		return tolls;		
	}
	public Date getTrip_end_timestamp(){
		return trip_end_timestamp;
	}
	public String getTrip_id(){
		return trip_id;
	}
	public double trip_miles(){
		return trip_miles;
	}
	public  Date trip_second(){
		return trip_seconds;
	}
	public String trip_start_timestamp(){
		return trip_start_timestamp;
	}
	public  double trip_total(){
		return trip_total;
	}

	public double getExtras() {
		return extras;
	}

	public void setExtras(double extras) {
		this.extras = extras;
	}

	public double getFare() {
		return fare;
	}

	public void setFare(double fare) {
		this.fare = fare;
	}

	public String getPayment_type() {
		return payment_type;
	}

	public void setPayment_type(String payment_type) {
		this.payment_type = payment_type;
	}

	public double getPickup_census_tract() {
		return pickup_census_tract;
	}

	public void setPickup_census_tract(double pickup_census_tract) {
		this.pickup_census_tract = pickup_census_tract;
	}

	public double getPickup_centroid_latitude() {
		return pickup_centroid_latitude;
	}

	public void setPickup_centroid_latitude(double pickup_centroid_latitude) {
		this.pickup_centroid_latitude = pickup_centroid_latitude;
	}

	public Point getPickup_centroid_location() {
		return pickup_centroid_location;
	}

	public void setPickup_centroid_location(Point  pickup_centroid_location) {
		this.pickup_centroid_location = pickup_centroid_location;
	}

	public double getPickup_centroid_longitude() {
		return pickup_centroid_longitude;
	}

	public void setPickup_centroid_longitude(double pickup_centroid_longitude) {
		this.pickup_centroid_longitude = pickup_centroid_longitude;
	}

	public double getPickup_community_area() {
		return pickup_community_area;
	}

	public void setPickup_community_area(double pickup_community_area) {
		this.pickup_community_area = pickup_community_area;
	}
	
	
	
	
	
		
	
	

}
